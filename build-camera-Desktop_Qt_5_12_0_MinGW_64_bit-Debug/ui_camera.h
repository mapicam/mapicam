/********************************************************************************
** Form generated from reading UI file 'camera.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERA_H
#define UI_CAMERA_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcameraviewfinder.h"

QT_BEGIN_NAMESPACE

class Ui_Camera
{
public:
    QAction *actionExit;
    QAction *actionStartCamera;
    QAction *actionStopCamera;
    QAction *actionSettings;
    QWidget *centralwidget;
    QLabel *label_9;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_7;
    QLabel *label_4;
    QFrame *line_2;
    QLabel *label_6;
    QLabel *label_8;
    QLabel *label_2;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_13;
    QStackedWidget *stackedWidget_2;
    QWidget *viewfinderPage_2;
    QVBoxLayout *verticalLayout_5;
    QCameraViewfinder *viewfinder_2;
    QWidget *previewPage_2;
    QGridLayout *gridLayout_5;
    QLabel *lastImagePreviewLabel_2;
    QHBoxLayout *horizontalLayout_2;
    QSpinBox *spinBox_2;
    QLineEdit *lineEdit_2;
    QSpinBox *spinBox_6;
    QToolButton *toolButton;
    QToolButton *toolButton_2;
    QToolButton *toolButton_3;
    QToolButton *toolButton_4;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_14;
    QStackedWidget *stackedWidget_3;
    QWidget *viewfinderPage_3;
    QVBoxLayout *verticalLayout_15;
    QCameraViewfinder *viewfinder_3;
    QWidget *previewPage_3;
    QGridLayout *gridLayout_8;
    QLabel *lastImagePreviewLabel_3;
    QHBoxLayout *horizontalLayout_3;
    QSpinBox *spinBox_3;
    QLineEdit *lineEdit_3;
    QSpinBox *spinBox_7;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_16;
    QStackedWidget *stackedWidget_4;
    QWidget *viewfinderPage_4;
    QVBoxLayout *verticalLayout_17;
    QCameraViewfinder *viewfinder_4;
    QWidget *previewPage_4;
    QGridLayout *gridLayout_9;
    QLabel *lastImagePreviewLabel_4;
    QHBoxLayout *horizontalLayout_4;
    QSpinBox *spinBox_4;
    QLineEdit *lineEdit_4;
    QSpinBox *spinBox_8;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_12;
    QStackedWidget *stackedWidget;
    QWidget *viewfinderPage;
    QVBoxLayout *verticalLayout;
    QCameraViewfinder *viewfinder;
    QWidget *previewPage;
    QGridLayout *gridLayout_4;
    QLabel *lastImagePreviewLabel;
    QHBoxLayout *horizontalLayout;
    QSpinBox *spinBox;
    QLineEdit *lineEdit;
    QSpinBox *spinBox_5;
    QTabWidget *captureWidget;
    QWidget *tab_2;
    QGridLayout *gridLayout;
    QSlider *exposureCompensation;
    QPushButton *pushButton;
    QPushButton *lockButton;
    QPushButton *pushButton_2;
    QSpacerItem *verticalSpacer_3;
    QLabel *label;
    QPushButton *takeImageButton;
    QSpacerItem *verticalSpacer_4;
    QWidget *tab;
    QGridLayout *gridLayout_2;
    QPushButton *recordButton;
    QPushButton *pauseButton;
    QPushButton *stopButton;
    QSpacerItem *verticalSpacer;
    QPushButton *muteButton;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuDevices;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Camera)
    {
        if (Camera->objectName().isEmpty())
            Camera->setObjectName(QString::fromUtf8("Camera"));
        Camera->resize(1339, 391);
        Camera->setMinimumSize(QSize(430, 300));
        Camera->setMaximumSize(QSize(3200, 1980));
        Camera->setBaseSize(QSize(430, 300));
        actionExit = new QAction(Camera);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionStartCamera = new QAction(Camera);
        actionStartCamera->setObjectName(QString::fromUtf8("actionStartCamera"));
        actionStopCamera = new QAction(Camera);
        actionStopCamera->setObjectName(QString::fromUtf8("actionStopCamera"));
        actionSettings = new QAction(Camera);
        actionSettings->setObjectName(QString::fromUtf8("actionSettings"));
        centralwidget = new QWidget(Camera);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(60, 320, 671, 16));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setEnabled(true);
        label_5->setGeometry(QRect(60, 280, 671, 16));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 260, 50, 16));
        label_3->setMinimumSize(QSize(50, 0));
        label_3->setMaximumSize(QSize(50, 16777215));
        label_3->setWordWrap(false);
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(60, 300, 671, 16));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 280, 50, 16));
        label_4->setMinimumSize(QSize(50, 0));
        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(10, 250, 1321, 16));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 300, 50, 16));
        label_6->setMinimumSize(QSize(50, 0));
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(10, 320, 50, 16));
        label_8->setMinimumSize(QSize(50, 0));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(60, 260, 671, 16));
        label_2->setMaximumSize(QSize(16777215, 16777215));
        verticalLayoutWidget_2 = new QWidget(centralwidget);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(430, 0, 302, 231));
        verticalLayout_13 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_13->setSpacing(0);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        verticalLayout_13->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_13->setContentsMargins(0, 0, 0, 0);
        stackedWidget_2 = new QStackedWidget(verticalLayoutWidget_2);
        stackedWidget_2->setObjectName(QString::fromUtf8("stackedWidget_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(stackedWidget_2->sizePolicy().hasHeightForWidth());
        stackedWidget_2->setSizePolicy(sizePolicy);
        stackedWidget_2->setMinimumSize(QSize(300, 200));
        stackedWidget_2->setMaximumSize(QSize(300, 200));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(145, 145, 145, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        stackedWidget_2->setPalette(palette);
        viewfinderPage_2 = new QWidget();
        viewfinderPage_2->setObjectName(QString::fromUtf8("viewfinderPage_2"));
        verticalLayout_5 = new QVBoxLayout(viewfinderPage_2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        viewfinder_2 = new QCameraViewfinder(viewfinderPage_2);
        viewfinder_2->setObjectName(QString::fromUtf8("viewfinder_2"));
        viewfinder_2->setMinimumSize(QSize(300, 200));
        viewfinder_2->setMaximumSize(QSize(300, 200));

        verticalLayout_5->addWidget(viewfinder_2, 0, Qt::AlignTop);

        stackedWidget_2->addWidget(viewfinderPage_2);
        previewPage_2 = new QWidget();
        previewPage_2->setObjectName(QString::fromUtf8("previewPage_2"));
        gridLayout_5 = new QGridLayout(previewPage_2);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        lastImagePreviewLabel_2 = new QLabel(previewPage_2);
        lastImagePreviewLabel_2->setObjectName(QString::fromUtf8("lastImagePreviewLabel_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lastImagePreviewLabel_2->sizePolicy().hasHeightForWidth());
        lastImagePreviewLabel_2->setSizePolicy(sizePolicy1);
        lastImagePreviewLabel_2->setMinimumSize(QSize(300, 200));
        lastImagePreviewLabel_2->setMaximumSize(QSize(300, 200));
        lastImagePreviewLabel_2->setFrameShape(QFrame::Box);

        gridLayout_5->addWidget(lastImagePreviewLabel_2, 0, 1, 1, 1);

        stackedWidget_2->addWidget(previewPage_2);

        verticalLayout_13->addWidget(stackedWidget_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, -1);
        spinBox_2 = new QSpinBox(verticalLayoutWidget_2);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        spinBox_2->setMinimumSize(QSize(40, 20));
        spinBox_2->setMaximumSize(QSize(40, 20));
        spinBox_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        spinBox_2->setMinimum(1);
        spinBox_2->setMaximum(120);
        spinBox_2->setSingleStep(1);
        spinBox_2->setValue(1);

        horizontalLayout_2->addWidget(spinBox_2);

        lineEdit_2 = new QLineEdit(verticalLayoutWidget_2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setMinimumSize(QSize(220, 20));
        lineEdit_2->setMaximumSize(QSize(220, 20));
        lineEdit_2->setMaxLength(128);

        horizontalLayout_2->addWidget(lineEdit_2);

        spinBox_6 = new QSpinBox(verticalLayoutWidget_2);
        spinBox_6->setObjectName(QString::fromUtf8("spinBox_6"));
        spinBox_6->setMinimumSize(QSize(20, 20));
        spinBox_6->setMaximumSize(QSize(40, 20));
        spinBox_6->setMaximum(360);
        spinBox_6->setValue(90);

        horizontalLayout_2->addWidget(spinBox_6);


        verticalLayout_13->addLayout(horizontalLayout_2);

        toolButton = new QToolButton(centralwidget);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setEnabled(true);
        toolButton->setGeometry(QRect(130, 230, 41, 19));
        toolButton_2 = new QToolButton(centralwidget);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));
        toolButton_2->setGeometry(QRect(430, 230, 41, 19));
        toolButton_3 = new QToolButton(centralwidget);
        toolButton_3->setObjectName(QString::fromUtf8("toolButton_3"));
        toolButton_3->setGeometry(QRect(730, 230, 41, 19));
        toolButton_4 = new QToolButton(centralwidget);
        toolButton_4->setObjectName(QString::fromUtf8("toolButton_4"));
        toolButton_4->setGeometry(QRect(1030, 230, 41, 19));
        checkBox = new QCheckBox(centralwidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setEnabled(true);
        checkBox->setGeometry(QRect(180, 230, 121, 17));
        checkBox_2 = new QCheckBox(centralwidget);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setGeometry(QRect(480, 230, 121, 17));
        checkBox_3 = new QCheckBox(centralwidget);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(780, 230, 121, 17));
        checkBox_4 = new QCheckBox(centralwidget);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));
        checkBox_4->setGeometry(QRect(1080, 230, 121, 17));
        verticalLayoutWidget_3 = new QWidget(centralwidget);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(730, 0, 302, 231));
        verticalLayout_14 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_14->setSpacing(0);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalLayout_14->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_14->setContentsMargins(0, 0, 0, 0);
        stackedWidget_3 = new QStackedWidget(verticalLayoutWidget_3);
        stackedWidget_3->setObjectName(QString::fromUtf8("stackedWidget_3"));
        sizePolicy.setHeightForWidth(stackedWidget_3->sizePolicy().hasHeightForWidth());
        stackedWidget_3->setSizePolicy(sizePolicy);
        stackedWidget_3->setMinimumSize(QSize(300, 200));
        stackedWidget_3->setMaximumSize(QSize(300, 200));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        stackedWidget_3->setPalette(palette1);
        viewfinderPage_3 = new QWidget();
        viewfinderPage_3->setObjectName(QString::fromUtf8("viewfinderPage_3"));
        verticalLayout_15 = new QVBoxLayout(viewfinderPage_3);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        viewfinder_3 = new QCameraViewfinder(viewfinderPage_3);
        viewfinder_3->setObjectName(QString::fromUtf8("viewfinder_3"));
        viewfinder_3->setMinimumSize(QSize(300, 200));
        viewfinder_3->setMaximumSize(QSize(300, 200));

        verticalLayout_15->addWidget(viewfinder_3, 0, Qt::AlignTop);

        stackedWidget_3->addWidget(viewfinderPage_3);
        previewPage_3 = new QWidget();
        previewPage_3->setObjectName(QString::fromUtf8("previewPage_3"));
        gridLayout_8 = new QGridLayout(previewPage_3);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        lastImagePreviewLabel_3 = new QLabel(previewPage_3);
        lastImagePreviewLabel_3->setObjectName(QString::fromUtf8("lastImagePreviewLabel_3"));
        sizePolicy1.setHeightForWidth(lastImagePreviewLabel_3->sizePolicy().hasHeightForWidth());
        lastImagePreviewLabel_3->setSizePolicy(sizePolicy1);
        lastImagePreviewLabel_3->setMinimumSize(QSize(300, 200));
        lastImagePreviewLabel_3->setMaximumSize(QSize(300, 200));
        lastImagePreviewLabel_3->setFrameShape(QFrame::Box);

        gridLayout_8->addWidget(lastImagePreviewLabel_3, 0, 1, 1, 1);

        stackedWidget_3->addWidget(previewPage_3);

        verticalLayout_14->addWidget(stackedWidget_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, -1);
        spinBox_3 = new QSpinBox(verticalLayoutWidget_3);
        spinBox_3->setObjectName(QString::fromUtf8("spinBox_3"));
        spinBox_3->setMinimumSize(QSize(40, 20));
        spinBox_3->setMaximumSize(QSize(40, 20));
        spinBox_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        spinBox_3->setMinimum(1);
        spinBox_3->setMaximum(120);
        spinBox_3->setSingleStep(1);
        spinBox_3->setValue(1);

        horizontalLayout_3->addWidget(spinBox_3);

        lineEdit_3 = new QLineEdit(verticalLayoutWidget_3);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setMinimumSize(QSize(220, 20));
        lineEdit_3->setMaximumSize(QSize(220, 20));
        lineEdit_3->setMaxLength(128);

        horizontalLayout_3->addWidget(lineEdit_3);

        spinBox_7 = new QSpinBox(verticalLayoutWidget_3);
        spinBox_7->setObjectName(QString::fromUtf8("spinBox_7"));
        spinBox_7->setMinimumSize(QSize(20, 20));
        spinBox_7->setMaximumSize(QSize(40, 20));
        spinBox_7->setMaximum(360);
        spinBox_7->setValue(180);

        horizontalLayout_3->addWidget(spinBox_7);


        verticalLayout_14->addLayout(horizontalLayout_3);

        verticalLayoutWidget_4 = new QWidget(centralwidget);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(1030, 0, 302, 231));
        verticalLayout_16 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_16->setSpacing(0);
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        verticalLayout_16->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_16->setContentsMargins(0, 0, 0, 0);
        stackedWidget_4 = new QStackedWidget(verticalLayoutWidget_4);
        stackedWidget_4->setObjectName(QString::fromUtf8("stackedWidget_4"));
        sizePolicy.setHeightForWidth(stackedWidget_4->sizePolicy().hasHeightForWidth());
        stackedWidget_4->setSizePolicy(sizePolicy);
        stackedWidget_4->setMinimumSize(QSize(300, 200));
        stackedWidget_4->setMaximumSize(QSize(300, 200));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Base, brush);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        stackedWidget_4->setPalette(palette2);
        viewfinderPage_4 = new QWidget();
        viewfinderPage_4->setObjectName(QString::fromUtf8("viewfinderPage_4"));
        verticalLayout_17 = new QVBoxLayout(viewfinderPage_4);
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        viewfinder_4 = new QCameraViewfinder(viewfinderPage_4);
        viewfinder_4->setObjectName(QString::fromUtf8("viewfinder_4"));
        viewfinder_4->setMinimumSize(QSize(300, 200));
        viewfinder_4->setMaximumSize(QSize(300, 200));

        verticalLayout_17->addWidget(viewfinder_4, 0, Qt::AlignTop);

        stackedWidget_4->addWidget(viewfinderPage_4);
        previewPage_4 = new QWidget();
        previewPage_4->setObjectName(QString::fromUtf8("previewPage_4"));
        gridLayout_9 = new QGridLayout(previewPage_4);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        lastImagePreviewLabel_4 = new QLabel(previewPage_4);
        lastImagePreviewLabel_4->setObjectName(QString::fromUtf8("lastImagePreviewLabel_4"));
        sizePolicy1.setHeightForWidth(lastImagePreviewLabel_4->sizePolicy().hasHeightForWidth());
        lastImagePreviewLabel_4->setSizePolicy(sizePolicy1);
        lastImagePreviewLabel_4->setMinimumSize(QSize(300, 200));
        lastImagePreviewLabel_4->setMaximumSize(QSize(300, 200));
        lastImagePreviewLabel_4->setFrameShape(QFrame::Box);

        gridLayout_9->addWidget(lastImagePreviewLabel_4, 0, 1, 1, 1);

        stackedWidget_4->addWidget(previewPage_4);

        verticalLayout_16->addWidget(stackedWidget_4);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, -1);
        spinBox_4 = new QSpinBox(verticalLayoutWidget_4);
        spinBox_4->setObjectName(QString::fromUtf8("spinBox_4"));
        spinBox_4->setMinimumSize(QSize(40, 20));
        spinBox_4->setMaximumSize(QSize(40, 20));
        spinBox_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        spinBox_4->setMinimum(1);
        spinBox_4->setMaximum(120);
        spinBox_4->setSingleStep(1);
        spinBox_4->setValue(1);

        horizontalLayout_4->addWidget(spinBox_4);

        lineEdit_4 = new QLineEdit(verticalLayoutWidget_4);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setMinimumSize(QSize(220, 20));
        lineEdit_4->setMaximumSize(QSize(220, 20));
        lineEdit_4->setMaxLength(128);

        horizontalLayout_4->addWidget(lineEdit_4);

        spinBox_8 = new QSpinBox(verticalLayoutWidget_4);
        spinBox_8->setObjectName(QString::fromUtf8("spinBox_8"));
        spinBox_8->setMinimumSize(QSize(20, 20));
        spinBox_8->setMaximumSize(QSize(40, 20));
        spinBox_8->setMaximum(360);
        spinBox_8->setValue(270);

        horizontalLayout_4->addWidget(spinBox_8);


        verticalLayout_16->addLayout(horizontalLayout_4);

        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(130, 0, 302, 231));
        verticalLayout_12 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_12->setSpacing(0);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        verticalLayout_12->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_12->setContentsMargins(0, 0, 0, 0);
        stackedWidget = new QStackedWidget(verticalLayoutWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        sizePolicy.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy);
        stackedWidget->setMinimumSize(QSize(300, 200));
        stackedWidget->setMaximumSize(QSize(300, 200));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::Base, brush);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        stackedWidget->setPalette(palette3);
        viewfinderPage = new QWidget();
        viewfinderPage->setObjectName(QString::fromUtf8("viewfinderPage"));
        verticalLayout = new QVBoxLayout(viewfinderPage);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        viewfinder = new QCameraViewfinder(viewfinderPage);
        viewfinder->setObjectName(QString::fromUtf8("viewfinder"));
        viewfinder->setMinimumSize(QSize(300, 200));
        viewfinder->setMaximumSize(QSize(300, 200));

        verticalLayout->addWidget(viewfinder, 0, Qt::AlignTop);

        stackedWidget->addWidget(viewfinderPage);
        previewPage = new QWidget();
        previewPage->setObjectName(QString::fromUtf8("previewPage"));
        gridLayout_4 = new QGridLayout(previewPage);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        lastImagePreviewLabel = new QLabel(previewPage);
        lastImagePreviewLabel->setObjectName(QString::fromUtf8("lastImagePreviewLabel"));
        sizePolicy1.setHeightForWidth(lastImagePreviewLabel->sizePolicy().hasHeightForWidth());
        lastImagePreviewLabel->setSizePolicy(sizePolicy1);
        lastImagePreviewLabel->setMinimumSize(QSize(300, 200));
        lastImagePreviewLabel->setMaximumSize(QSize(300, 200));
        lastImagePreviewLabel->setFrameShape(QFrame::Box);

        gridLayout_4->addWidget(lastImagePreviewLabel, 0, 1, 1, 1);

        stackedWidget->addWidget(previewPage);

        verticalLayout_12->addWidget(stackedWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, -1);
        spinBox = new QSpinBox(verticalLayoutWidget);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMinimumSize(QSize(40, 20));
        spinBox->setMaximumSize(QSize(32, 20));
        spinBox->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        spinBox->setMinimum(1);
        spinBox->setMaximum(120);
        spinBox->setSingleStep(1);
        spinBox->setValue(1);

        horizontalLayout->addWidget(spinBox);

        lineEdit = new QLineEdit(verticalLayoutWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setMinimumSize(QSize(220, 20));
        lineEdit->setMaximumSize(QSize(220, 20));
        lineEdit->setMaxLength(128);

        horizontalLayout->addWidget(lineEdit);

        spinBox_5 = new QSpinBox(verticalLayoutWidget);
        spinBox_5->setObjectName(QString::fromUtf8("spinBox_5"));
        spinBox_5->setMinimumSize(QSize(20, 20));
        spinBox_5->setMaximumSize(QSize(40, 20));
        spinBox_5->setMaximum(360);

        horizontalLayout->addWidget(spinBox_5);


        verticalLayout_12->addLayout(horizontalLayout);

        captureWidget = new QTabWidget(centralwidget);
        captureWidget->setObjectName(QString::fromUtf8("captureWidget"));
        captureWidget->setGeometry(QRect(10, 10, 120, 220));
        captureWidget->setMinimumSize(QSize(120, 220));
        captureWidget->setMaximumSize(QSize(120, 220));
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout = new QGridLayout(tab_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        exposureCompensation = new QSlider(tab_2);
        exposureCompensation->setObjectName(QString::fromUtf8("exposureCompensation"));
        exposureCompensation->setMinimumSize(QSize(100, 20));
        exposureCompensation->setMaximumSize(QSize(100, 20));
        exposureCompensation->setBaseSize(QSize(100, 20));
        exposureCompensation->setMinimum(-4);
        exposureCompensation->setMaximum(4);
        exposureCompensation->setPageStep(2);
        exposureCompensation->setOrientation(Qt::Horizontal);
        exposureCompensation->setTickPosition(QSlider::TicksAbove);

        gridLayout->addWidget(exposureCompensation, 6, 0, 1, 1);

        pushButton = new QPushButton(tab_2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setMinimumSize(QSize(100, 32));
        pushButton->setMaximumSize(QSize(100, 32));

        gridLayout->addWidget(pushButton, 1, 0, 1, 1);

        lockButton = new QPushButton(tab_2);
        lockButton->setObjectName(QString::fromUtf8("lockButton"));
        lockButton->setMinimumSize(QSize(100, 20));
        lockButton->setMaximumSize(QSize(100, 20));

        gridLayout->addWidget(lockButton, 4, 0, 1, 1);

        pushButton_2 = new QPushButton(tab_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(100, 32));
        pushButton_2->setMaximumSize(QSize(100, 32));

        gridLayout->addWidget(pushButton_2, 2, 0, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 7, 0, 1, 1);

        label = new QLabel(tab_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(100, 20));
        label->setMaximumSize(QSize(100, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setPointSize(8);
        label->setFont(font);

        gridLayout->addWidget(label, 5, 0, 1, 1);

        takeImageButton = new QPushButton(tab_2);
        takeImageButton->setObjectName(QString::fromUtf8("takeImageButton"));
        takeImageButton->setEnabled(false);
        takeImageButton->setMinimumSize(QSize(100, 32));
        takeImageButton->setMaximumSize(QSize(100, 32));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/shutter.svg"), QSize(), QIcon::Normal, QIcon::Off);
        takeImageButton->setIcon(icon);

        gridLayout->addWidget(takeImageButton, 0, 0, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 3, 0, 1, 1);

        captureWidget->addTab(tab_2, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_2 = new QGridLayout(tab);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        recordButton = new QPushButton(tab);
        recordButton->setObjectName(QString::fromUtf8("recordButton"));

        gridLayout_2->addWidget(recordButton, 0, 0, 1, 1);

        pauseButton = new QPushButton(tab);
        pauseButton->setObjectName(QString::fromUtf8("pauseButton"));

        gridLayout_2->addWidget(pauseButton, 1, 0, 1, 1);

        stopButton = new QPushButton(tab);
        stopButton->setObjectName(QString::fromUtf8("stopButton"));

        gridLayout_2->addWidget(stopButton, 2, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 76, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 3, 0, 1, 1);

        muteButton = new QPushButton(tab);
        muteButton->setObjectName(QString::fromUtf8("muteButton"));
        muteButton->setCheckable(true);

        gridLayout_2->addWidget(muteButton, 4, 0, 1, 1);

        captureWidget->addTab(tab, QString());
        Camera->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Camera);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1339, 21));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuDevices = new QMenu(menubar);
        menuDevices->setObjectName(QString::fromUtf8("menuDevices"));
        Camera->setMenuBar(menubar);
        statusbar = new QStatusBar(Camera);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        Camera->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuDevices->menuAction());
        menuFile->addAction(actionStartCamera);
        menuFile->addAction(actionStopCamera);
        menuFile->addSeparator();
        menuFile->addAction(actionSettings);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);

        retranslateUi(Camera);
        QObject::connect(recordButton, SIGNAL(clicked()), Camera, SLOT(record()));
        QObject::connect(stopButton, SIGNAL(clicked()), Camera, SLOT(stop()));
        QObject::connect(pauseButton, SIGNAL(clicked()), Camera, SLOT(pause()));
        QObject::connect(actionExit, SIGNAL(triggered()), Camera, SLOT(close()));
        QObject::connect(takeImageButton, SIGNAL(clicked()), Camera, SLOT(takeImage()));
        QObject::connect(lockButton, SIGNAL(clicked()), Camera, SLOT(toggleLock()));
        QObject::connect(muteButton, SIGNAL(toggled(bool)), Camera, SLOT(setMuted(bool)));
        QObject::connect(exposureCompensation, SIGNAL(valueChanged(int)), Camera, SLOT(setExposureCompensation(int)));
        QObject::connect(actionSettings, SIGNAL(triggered()), Camera, SLOT(configureCaptureSettings()));
        QObject::connect(actionStartCamera, SIGNAL(triggered()), Camera, SLOT(startCamera()));
        QObject::connect(actionStopCamera, SIGNAL(triggered()), Camera, SLOT(stopCamera()));

        stackedWidget_2->setCurrentIndex(0);
        stackedWidget_3->setCurrentIndex(0);
        stackedWidget_4->setCurrentIndex(0);
        stackedWidget->setCurrentIndex(0);
        captureWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Camera);
    } // setupUi

    void retranslateUi(QMainWindow *Camera)
    {
        Camera->setWindowTitle(QApplication::translate("Camera", "Camera", nullptr));
        actionExit->setText(QApplication::translate("Camera", "Exit", nullptr));
        actionStartCamera->setText(QApplication::translate("Camera", "Start Camera", nullptr));
        actionStopCamera->setText(QApplication::translate("Camera", "Stop Camera", nullptr));
        actionSettings->setText(QApplication::translate("Camera", "Settings", nullptr));
        label_9->setText(QApplication::translate("Camera", "33333.jpg", nullptr));
        label_5->setText(QApplication::translate("Camera", "11111.jpg", nullptr));
        label_3->setText(QApplication::translate("Camera", "cam-00", nullptr));
        label_7->setText(QApplication::translate("Camera", "22222.jpg", nullptr));
        label_4->setText(QApplication::translate("Camera", "cam-01", nullptr));
        label_6->setText(QApplication::translate("Camera", "cam-02", nullptr));
        label_8->setText(QApplication::translate("Camera", "cam-03", nullptr));
        label_2->setText(QApplication::translate("Camera", "none", nullptr));
        lastImagePreviewLabel_2->setText(QString());
        lineEdit_2->setText(QApplication::translate("Camera", "MapiCam-01-", nullptr));
        toolButton->setText(QApplication::translate("Camera", "...", nullptr));
        toolButton_2->setText(QApplication::translate("Camera", "...", nullptr));
        toolButton_3->setText(QApplication::translate("Camera", "...", nullptr));
        toolButton_4->setText(QApplication::translate("Camera", "...", nullptr));
        checkBox->setText(QApplication::translate("Camera", "\320\260\320\272\321\202\320\270\320\262\321\203\320\262\320\260\321\202\320\270 \320\272\320\260\320\274\320\265\321\200\321\203", nullptr));
        checkBox_2->setText(QApplication::translate("Camera", "\320\260\320\272\321\202\320\270\320\262\321\203\320\262\320\260\321\202\320\270 \320\272\320\260\320\274\320\265\321\200\321\203", nullptr));
        checkBox_3->setText(QApplication::translate("Camera", "\320\260\320\272\321\202\320\270\320\262\321\203\320\262\320\260\321\202\320\270 \320\272\320\260\320\274\320\265\321\200\321\203", nullptr));
        checkBox_4->setText(QApplication::translate("Camera", "\320\260\320\272\321\202\320\270\320\262\321\203\320\262\320\260\321\202\320\270 \320\272\320\260\320\274\320\265\321\200\321\203", nullptr));
        lastImagePreviewLabel_3->setText(QString());
        lineEdit_3->setText(QApplication::translate("Camera", "MapiCam-02-", nullptr));
        lastImagePreviewLabel_4->setText(QString());
        lineEdit_4->setText(QApplication::translate("Camera", "MapiCam-03-", nullptr));
        lastImagePreviewLabel->setText(QString());
        lineEdit->setText(QApplication::translate("Camera", "MapiCam-00-", nullptr));
        pushButton->setText(QApplication::translate("Camera", "Start auto record", nullptr));
        lockButton->setText(QApplication::translate("Camera", "Focus", nullptr));
        pushButton_2->setText(QApplication::translate("Camera", "Stop auto record", nullptr));
        label->setText(QApplication::translate("Camera", "Exp. Compensation:", nullptr));
        takeImageButton->setText(QApplication::translate("Camera", "Capture Photo", nullptr));
        captureWidget->setTabText(captureWidget->indexOf(tab_2), QApplication::translate("Camera", "Image", nullptr));
        recordButton->setText(QApplication::translate("Camera", "Record", nullptr));
        pauseButton->setText(QApplication::translate("Camera", "Pause", nullptr));
        stopButton->setText(QApplication::translate("Camera", "Stop", nullptr));
        muteButton->setText(QApplication::translate("Camera", "Mute", nullptr));
        captureWidget->setTabText(captureWidget->indexOf(tab), QApplication::translate("Camera", "Video", nullptr));
        menuFile->setTitle(QApplication::translate("Camera", "File", nullptr));
        menuDevices->setTitle(QApplication::translate("Camera", "Devices", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Camera: public Ui_Camera {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERA_H
